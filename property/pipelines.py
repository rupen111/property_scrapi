import psycopg2


class PropertyPipeline(object):
    def __init__(self):
        self.conn = psycopg2.connect(user="rupendra",
                                     password='rupendra',
                                     dbname="property",
                                     host='localhost')

    def process_item(self, item, spider):
        cur = self.conn.cursor()
        cur.execute('''
                   insert into makaan ( apartment_name, bhk, latitude, longitude, area_city,
                      price, area_in_sq_ft, status, details, agent_builder )
                   values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
                   ''', [
            item['apartment_name'],
            item['bhk'],
            item['latitude'],
            item['longitude'],
            item['area_city'],
            item['price'],
            item['area_in_sq_ft'],
            item['status'],
            item['details'],
            item['agent_builder']
        ])
        self.conn.commit()
        return item
