# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class PropertyItem(scrapy.Item):
    apartment_name = scrapy.Field()
    bhk = scrapy.Field()
    latitude = scrapy.Field()
    longitude = scrapy.Field()
    area_city = scrapy.Field()
    price = scrapy.Field()
    area_in_sq_ft = scrapy.Field()
    status = scrapy.Field()
    details = scrapy.Field()
    agent_builder = scrapy.Field()
