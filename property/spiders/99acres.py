import scrapy
from property.items import PropertyItem


class ToScrapeSpiderXPath(scrapy.Spider):
    name = '99acres-xpath'
    url = [
        'https://www.99acres.com/buy-residential-apartments-in-surat-ffid',
    ]
    next_url_temp = 'https://www.99acres.com/buy-residential-apartments-in-surat-ffid?page= '

    custom_settings = {
        "DOWNLOAD_DELAY": 1,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 1
    }

    for i in range(1, 7):
        url.append(next_url_temp + str(i))
    start_urls = url

    def parse(self, response):
        items = PropertyItem()
        for obj in response.xpath('/html/body/div[1]/div/div/div[2]/div[2]/div[2]/div'):
            apartment_name = obj.xpath('./div[1]/div[2]/table/tr[2]/td/a/text()').extract_first(),
            bhk = obj.xpath('./div[1]/div[2]/table/tr[3]/td[3]/text()').extract_first()[0],
            latitude = None
            longitude = None
            area_city = obj.xpath('./div[1]/div[2]/table/tr[1]/td/a/h2/text()').extract_first()[30:],
            price = obj.xpath('./div[1]/div[2]/table/tr[3]/td[1]/text()').extract_first().replace('u', ''),
            area_in_sq_ft = obj.xpath('./div[1]/div[2]/table/tr[3]/td[2]/text()').extract_first(),
            status = obj.xpath('./div[1]/div[2]/table/tr[5]/td/div/div/text()').extract_first(),
            details = obj.xpath('./div[1]/div[2]/table/tr[1]/td/a/h2/text()').extract_first(),
            agent_builder = obj.xpath('./div[2]/div[1]/div/div[2]/a/text()').extract_first(),

            items['apartment_name'] = apartment_name
            items['bhk'] = bhk
            items['latitude'] = latitude
            items['longitude'] = longitude
            items['area_city'] = area_city
            items['price'] = price
            items['area_in_sq_ft'] = area_in_sq_ft
            items['status'] = status
            items['details'] = details
            items['agent_builder'] = agent_builder
            yield items
