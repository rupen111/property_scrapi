import scrapy
from property.items import PropertyItem


class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'magic-xpath'
    url = [
        'https://www.magicbricks.com/property-for-sale/residential-real-estate?proptype=Multistorey-Apartment,Builder-Floor-Apartment,Penthouse,Studio-Apartment&cityName=Surat',
    ]
    next_url_temp = 'https://www.magicbricks.com/property-for-sale/residential-real-estate?proptype=Multistorey-Apartment,Builder-Floor-Apartment,Penthouse,Studio-Apartment&cityName=Surat?page='

    custom_settings = {
        "DOWNLOAD_DELAY": 1,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 1
    }

    for i in range(1, 1):
        url.append(next_url_temp + str(i))
    start_urls = url

    def parse(self, response):
        items = PropertyItem()
        for obj in response.xpath('/html/body/div[10]/div[5]/div[3]/div[1]/div[1]/div[2]/div[2]/div[1]/div[3]/div'):
            apartment_name1 = './div[4]/div[3]/div[1]/h3/span/text()'
            apartment_name2 = './div[4]/div[3]/div[1]/h3/span/text()'
            apartment_name3 = './div[4]/div[3]/div[1]/h3/span/text()'
            apartment_name = obj.xpath(
                apartment_name1 + " | " + apartment_name2 + " | " + apartment_name3).extract_first(),

            bhk1 = '/div[4]/div[3]/div[1]/h3/span/span/text()'
            bhk2 = './div[3]/div[3]/div[1]/h3/span/span/text()'
            bhk3 = './div[2]/div[3]/div[1]/h3/span/span/text()'
            bhk = obj.xpath(bhk1 + " | " + bhk2 + " | " + bhk3).extract_first(),
            latitude1 = './div[4]/div[3]/div[1]/div/@onclick'
            latitude2 = './div[3]/div[1]/div/@onclick'
            latitude3 = './div[3]/div[1]/div/@onclick'
            latitude = obj.xpath(
                latitude1 + " | " + latitude2 + " | " + latitude3).extract_first(),

            longitude = None
            area_city = None
            price1 = './div[2]/div[2]/div/text()'
            price2 = './div[3]/div[2]/div/text()'
            price3 = './div[4]/div[2]/div/text()'

            price = obj.xpath(
                price1 + " | " + price2 + " | " + price3).extract_first(),

            area_in_sq_ft1 = './div[2]/div[3]/div[2]/div[1]/div[1]/div[2]/text()'
            area_in_sq_ft2 = './div[3]/div[3]/div[2]/div[1]/div[1]/div[2]/text()'
            area_in_sq_ft3 = './div[4]/div[3]/div[2]/div[1]/div[1]/div[2]/text()'
            area_in_sq_ft = obj.xpath(area_in_sq_ft1 + " | " + area_in_sq_ft2 + " | " + area_in_sq_ft3).extract_first(),
            status1 = './div[2]/div[3]/div[2]/div[1]/div[2]/div[2]/text()'
            status2 = './div[3]/div[3]/div[2]/div[1]/div[2]/div[2]/text()'
            status3 = './div[4]/div[3]/div[2]/div[1]/div[2]/div[2]/text()'
            status = obj.xpath(
                status1 + " | " + status2 + " | " + status3).extract_first(),

            details = None

            agent_builder1 = './div[2]/div[3]/div[5]/div[5]/div[1]/div/text()'
            agent_builder2 = './div[3]/div[3]/div[5]/div[5]/div[2]/text()'
            agent_builder3 = './div[4]/div[3]/div[5]/div[5]/div[2]/text()'
            agent_builder4 = './div[3]/div[3]/div[5]/div[4]/div[2]/text()'
            agent_builder5 = './div[3]/div[3]/div[4]/div[5]/div[2]/text()'
            agent_builder6 = './div[2]/div[3]/div[4]/div[5]/div[2]/text()'

            agent_builder = obj.xpath(
                agent_builder1 + " | " + agent_builder2 + " | " + agent_builder3 + " | " + agent_builder4 + " | " + agent_builder5 + " | " + agent_builder6).extract_first(),

            items['apartment_name'] = apartment_name
            items['bhk'] = bhk
            items['latitude'] = latitude
            items['longitude'] = longitude
            items['area_city'] = area_city
            items['price'] = price
            items['area_in_sq_ft'] = area_in_sq_ft
            items['status'] = status
            items['details'] = details
            items['agent_builder'] = agent_builder
            yield items
