# -*- coding: utf-8 -*-
import scrapy
from property.items import PropertyItem


class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'makaan-xpath'
    url = ['https://www.makaan.com/surat-residential-property/buy-property-in-surat-city']
    next_url_temp = 'https://www.makaan.com/surat-residential-property/buy-property-in-surat-city?page='

    for i in range(1, 1):
        url.append(next_url_temp + str(i))
    start_urls = url

    def parse(self, response):
        items = PropertyItem()

        for obj in response.xpath('/html/body/div[1]/main/div/div/div[2]/div/div[2]/div/div[1]/div[2]/ul/li'):
            extra_fields = ""
            for li_obj in obj.xpath('./div/div/div[3]/ul/li'):
                if li_obj.xpath('./span[1]/text()').extract_first():
                    extra_fields = extra_fields + "|" + \
                                   li_obj.xpath('./span[1]/text()').extract_first()
            if obj.xpath('./div/div/div[3]/table/tbody/tr[2]/td[1]/div/span[1]/meta[2]/@content').extract_first():
                apartment_name = obj.xpath(
                    './div/div/div[3]/div[1]/div[1]/span/strong/a/@data-link-name').extract_first(),
                bhk = obj.xpath('./div/div/div[3]/div[1]/div[1]/a/strong/span[1]/text()').extract_first(),
                latitude = obj.xpath('./div/div/div[1]/meta[1]/@content').extract_first(),
                longitude = obj.xpath('./div/div/div[1]/meta[2]/@content').extract_first(),
                area_city = obj.xpath(
                    './div/div/div[3]/div[1]/div[2]/div/span/span/span/a/@data-link-name').extract_first(),
                price = obj.xpath(
                    './div/div/div[3]/table/tbody/tr[2]/td[1]/div/span[1]/meta[2]/@content').extract_first(),
                area_in_sq_ft = obj.xpath('./div/div/div[3]/table/tbody/tr[3]/td[1]/span/text()').extract_first(),
                status = obj.xpath('./div/div/div[3]/table/tbody/tr[4]/td[1]/text()').extract_first(),
                details = obj.xpath('./div/div/div[3]/ul/text()').extract_first(),
                agent_builder = obj.xpath('./div/div/div[2]/div[3]/div/div[2]/a/@data-link-name').extract_first(),

                items['apartment_name'] = apartment_name
                items['bhk'] = bhk
                items['latitude'] = latitude
                items['longitude'] = longitude
                items['area_city'] = area_city
                items['price'] = price
                items['area_in_sq_ft'] = area_in_sq_ft
                items['status'] = status
                items['details'] = details
                items['agent_builder'] = agent_builder

                yield items
