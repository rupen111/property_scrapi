import scrapy
from property.items import PropertyItem


class ToScrapeSpiderXPath(scrapy.Spider):
    name = 'olx-xpath'
    url = [
        'https://www.olx.in/surat_g4058727/for-sale-houses-apartments_c1725',
    ]
    next_url_temp = 'https://www.olx.in/surat_g4058727/for-sale-houses-apartments_c1725?page='

    custom_settings = {
        "DOWNLOAD_DELAY": 1,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 1
    }

    for i in range(1, 1):
        url.append(next_url_temp + str(i))
    start_urls = url

    def parse(self, response):
        items = PropertyItem()
        for obj in response.xpath('/html/body/div[1]/div/main/div/section/div/div/div[4]/div[2]/div/div[2]/ul/li'):
            apartment_name = obj.xpath('./a/div/div/span/text()').extract_first(),
            bhk = obj.xpath('./a/div/span[2]/text()').extract_first()[0],
            latitude = None
            longitude = None
            area_city = obj.xpath('./a/div/div/span/text()').extract_first(),
            price = obj.xpath('./a/div/span[1]/text()').extract_first()[1:].replace(',', ''),
            area_in_sq_ft = obj.xpath('./a/div/span[2]/text()').extract_first()[15:19],
            status = None
            details = obj.xpath('./a/div/span[3]/text()').extract_first(),
            agent_builder = None

            items['apartment_name'] = apartment_name
            items['bhk'] = bhk
            items['latitude'] = latitude
            items['longitude'] = longitude
            items['area_city'] = area_city
            items['price'] = price
            items['area_in_sq_ft'] = area_in_sq_ft
            items['status'] = status
            items['details'] = details
            items['agent_builder'] = agent_builder
            yield items
